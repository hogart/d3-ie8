'use strict';

angular.module('scoreBoardApp', [], function () {})
    .factory('players', ['$rootScope', playersFactory])
    .controller('PlayersController', ['players', PlayersController])
    .controller('ScoresController', ['players', ScoresController])
    .controller('ChartController', ['$element', '$scope', ChartController]);
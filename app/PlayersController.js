function PlayersController (players) {
    this.players = players;
    this.list = players._list;
    this.newPlayerName = '';
}

PlayersController.prototype.delPlayer = function delPlayer (index) {
    this.players.delPlayer(index);
};

PlayersController.prototype.addPlayer = function addPlayer () {
    this.newPlayerName = this.newPlayerName.trim();

    if (this.newPlayerName) {
        this.players.addPlayer(this.newPlayerName);
    }
};
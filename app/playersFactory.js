function playersFactory ($rootScope) {
    var emit = $rootScope.$broadcast.bind($rootScope, 'players:updated');

    return new PlayersCollection(emit);
}

function PlayersCollection (emit) {
    this._list = [];
    this.emit = emit.bind(null, this._list);
}

PlayersCollection.prototype = {
    delPlayer: function (index) {
        this._list.splice(index, 1);
        this.emit();
    },

    addScore: function (index, amount) {
        this._list[index].score += amount;
        this.emit();
    },

    addPlayer: function (name) {
        this._list.push({name: name, score: 0});
        this.emit();
    }
};
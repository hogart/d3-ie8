var WIDTH = 300;
var HEIGHT = 300;
var RADIUS = Math.min(WIDTH, HEIGHT) / 2;

function ChartController ($element, $scope) {
    this._arc = d3.svg.arc()
        .outerRadius(RADIUS * 0.9)
        .innerRadius(RADIUS * 0.7);

    this._pie = d3.layout.pie()
        .sort(null)
        .value(function(d) { return d.score; });

    this._svg = d3.select($element[0]).append('svg')
        .attr('width', WIDTH)
        .attr('height', HEIGHT)
        .append('g')
        .attr('transform', 'translate(' + WIDTH / 2 + ',' + HEIGHT / 2 + ')');

    this._color = d3.scale.ordinal().range(this._availableColors);

    $scope.$on('players:updated', this.updateChart.bind(this));
}

ChartController.prototype._availableColors = ['#98abc5', '#8a89a6', '#7b6888', '#6b486b', '#a05d56', '#d0743c', '#ff8c00'];

ChartController.prototype.updateChart = function updateChart (params, data) {
    var players = data.filter(function (player) {
        return player.score !== 0;
    });

    var arc = this._arc;
    var color = this._color;

    this._svg.selectAll('g, shape').remove();

    var g = this._svg.selectAll('.arc')
        .data(this._pie(players));

    g.enter()
        .append('g')
            .attr('class', 'arc')
        .append('path')
            .attr('d', this._arc)
            .style('fill', function (d) {
                return color(d.data.name);
            });

    g.append('text')
        .attr('transform', function (d) {
            return 'translate(' + arc.centroid(d) + ')';
        })
        .attr('y', '.35em')
        .style('text-anchor', 'middle')
        .text(function(d) {
            return d.data.name + ': ' + d.data.score;
        });


    g.exit()
        .remove();
};
function ScoresController (players) {
    this.players = players;
    this.list = players._list;
    this.addScoreAmount = 0;
}

ScoresController.prototype.addScore = function addScore (index) {
    if (this.addScoreAmount) {
        this.players.addScore(index, parseInt(this.addScoreAmount));
    }
};